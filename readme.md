# Data Warehousing

This repo is intended to store all material related to Data Warehousing

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.
### Prerequisites

What things you need to install the software and how to install them
 - Postgresql
 - Pentaho Data Integration
 - DBeaver

### Installing

On this section is presented a set of instructions in order to run this project localy

#### Database

Proper configuration of the hosting database. Enter your postgres command line tool and run the following commands

```sql
CREATE DATABASE DW_TP;
CREATE USER so WITH PASSWORD 'so' LOGIN;
CREATE SCHEMA AUTHORIZATION so;
CREATE USER stage WITH PASSWORD 'stage' LOGIN;
CREATE SCHEMA AUTHORIZATION stage;
CREATE USER dw WITH PASSWORD 'dw' LOGIN;
CREATE SCHEMA AUTHORIZATION dw;
```

#### Pentaho Data Integration

Instructions to run the project with Data Integration

```

```