
-- DROP DIMENSIONS AND FACTS

DROP TABLE IF EXISTS dim_animais;

DROP TABLE IF EXISTS dim_clientes;

DROP TABLE IF EXISTS dim_colaboradores;

DROP TABLE IF EXISTS dim_cond_saude;

DROP TABLE IF EXISTS dim_local;

-- DROP TABLE IF EXISTS dim_tempo;

DROP TABLE IF EXISTS fact_adocoes;

DROP TABLE IF EXISTS fact_recolhas;

-- DROP EXTRACTIONS

DROP TABLE IF EXISTS adocoes;

DROP TABLE IF EXISTS animais;

DROP TABLE IF EXISTS clientes;

DROP TABLE IF EXISTS colaboradores;

DROP TABLE IF EXISTS condicoes_saude;

DROP TABLE IF EXISTS locais;

DROP TABLE IF EXISTS recolhas;

-- DROP MAPPINGS

DROP TABLE IF EXISTS animais_mapper;

DROP TABLE IF EXISTS clientes_mapper;

DROP TABLE IF EXISTS colab_mapper;

DROP TABLE IF EXISTS cond_saude_mapper;

DROP TABLE IF EXISTS local_mapper;

-- DROP DATASETS

DROP TABLE IF EXISTS cd_locais;

-- End of file.