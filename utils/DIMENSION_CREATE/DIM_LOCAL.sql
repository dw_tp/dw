CREATE TABLE dim_local
(
  cod_distrito INTEGER
, cod_concelho INTEGER
, cod_localidade INTEGER
, nome_localidade VARCHAR(100)
, cod_arteria INTEGER
, tipo_arteria VARCHAR(100)
, prep1 VARCHAR(20)
, titulo_arteria VARCHAR(20)
, prep2 VARCHAR(20)
, nome_arteria VARCHAR(100)
, local_arteria VARCHAR(100)
, troco VARCHAR(50)
, porta VARCHAR(50)
, num_cod_postal INTEGER
, ext_cod_postal INTEGER
, desig_postal VARCHAR(100)
, nome_concelho VARCHAR(200)
, nome_distrito VARCHAR(200)
, id_local DOUBLE PRECISION
, latitude TEXT
, longitude TEXT
)
;