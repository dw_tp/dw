CREATE TABLE dim_clientes
(
  nome VARCHAR(50)
, morada VARCHAR(50)
, cod_postal VARCHAR(40)
, localidade VARCHAR(40)
, data_nascimento TIMESTAMP
, data_inscricao TIMESTAMP
, sexo TEXT
, profissao TEXT
, estado_civil TEXT
, id_cliente DOUBLE PRECISION
)
;