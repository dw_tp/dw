CREATE TABLE dim_animais
(
  raca TEXT
, cor_pelo TEXT
, numero_patas INTEGER
, sexo TEXT
, nome VARCHAR(50)
, data_nascimento TIMESTAMP
, data_entrada TIMESTAMP
, altura INTEGER
, comprimento_pelagem INTEGER
, altura_pelagem INTEGER
, cauda TEXT
, local_microchip TEXT
, codigo_microchip VARCHAR(50)
, sinais_particulares VARCHAR(50)
, id_animal DOUBLE PRECISION
)
;