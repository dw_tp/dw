CREATE TABLE dim_tempo
(
  date_field TIMESTAMP
, "year" SMALLINT
, "month" SMALLINT
, "day" SMALLINT
, day_of_week_US SMALLINT
, date_tk DOUBLE PRECISION
, ymd TEXT
, ym TEXT
, quarter SMALLINT
, quarter_code VARCHAR(2)
, week SMALLINT
, day_of_year SMALLINT
, day_of_week VARCHAR(1)
, month_desc VARCHAR(20)
, month_code VARCHAR(3)
, day_of_week_desc VARCHAR(20)
, day_of_week_code VARCHAR(3)
, "hour" SMALLINT
, "minute" SMALLINT
, time_tk INTEGER
, ampm VARCHAR(8)
, hour12 SMALLINT
, hms VARCHAR(8)
, hm VARCHAR(5)
, hms12 VARCHAR(20)
, id_tempo DOUBLE PRECISION
)
;