
-- DROP DIMENSIONS AND FACTS

DROP TABLE IF EXISTS dim_animais;

DROP TABLE IF EXISTS dim_clientes;

DROP TABLE IF EXISTS dim_colaboradores;

DROP TABLE IF EXISTS dim_cond_saude;

DROP TABLE IF EXISTS dim_local;

-- DROP TABLE IF EXISTS dim_tempo;

DROP TABLE IF EXISTS fact_adocoes;

DROP TABLE IF EXISTS fact_recolhas;

-- End of file.