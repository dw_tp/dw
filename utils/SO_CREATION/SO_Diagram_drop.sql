-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-12-26 18:12:40.71

-- foreign keys
ALTER TABLE IF EXISTS Adocao
    DROP CONSTRAINT IF EXISTS Adocao_Animal;

ALTER TABLE IF EXISTS Adocao
    DROP CONSTRAINT IF EXISTS Adocao_Cliente;

ALTER TABLE IF EXISTS Adocao
    DROP CONSTRAINT IF EXISTS Adocao_Colaborador;

ALTER TABLE IF EXISTS Adocao
    DROP CONSTRAINT IF EXISTS Adocao_Condicao_saude;

ALTER TABLE IF EXISTS Recolha
    DROP CONSTRAINT IF EXISTS Recolha_Animal;

ALTER TABLE IF EXISTS Recolha
    DROP CONSTRAINT IF EXISTS Recolha_Colaborador;

ALTER TABLE IF EXISTS Recolha
    DROP CONSTRAINT IF EXISTS Recolha_Condicao_saude;

ALTER TABLE IF EXISTS Recolha
    DROP CONSTRAINT IF EXISTS Recolha_Local;

ALTER TABLE IF EXISTS Colaborador
    DROP CONSTRAINT IF EXISTS Colaborador_Local;

ALTER TABLE IF EXISTS Cliente
    DROP CONSTRAINT IF EXISTS Cliente_Local;

-- tables
DROP TABLE IF EXISTS Adocao;

DROP TABLE IF EXISTS Animal;

DROP TABLE IF EXISTS Cliente;

DROP TABLE IF EXISTS Colaborador;

DROP TABLE IF EXISTS Recolha;

DROP TABLE IF EXISTS Condicao_saude;

DROP TABLE IF EXISTS "local";

-- End of file.

