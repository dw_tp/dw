-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-12-26 18:12:40.71

-- tables

-- Table: Local
CREATE TABLE "local" (
  id SERIAL NOT NULL,
  cod_distrito int,
  cod_concelho int,
  cod_localidade int,
  nome_localidade varchar(100) NOT NULL,
  cod_arteria int,
  tipo_arteria varchar(100),
  prep1 varchar(20),
  titulo_arteria varchar(20),
  prep2 varchar(20),
  nome_arteria varchar(100),
  local_arteria varchar(100),
  troco varchar(50),
  porta varchar(50),
  num_cod_postal int,
  ext_cod_postal int,
  desig_postal varchar(100),
  nome_concelho varchar(100),
  nome_distrito varchar(100),
  CONSTRAINT Local_pk PRIMARY KEY (id)
);

-- Table: Adocao
CREATE TABLE Condicao_saude (
    id SERIAL NOT NULL,
    nivel varchar(10) NOT NULL,
    temperatura int NOT NULL,
    sintomas varchar(500) NOT NULL,
    descricao varchar(200) NOT NULL,
    CONSTRAINT Condicao_saude_pk PRIMARY KEY (id)
);

-- Table: Adocao
CREATE TABLE Adocao (
    id SERIAL  NOT NULL,
    id_cliente int  NOT NULL,
    id_colaborador int  NOT NULL,
    id_animal int  NOT NULL,
    data date  NOT NULL,
    id_condicao_saude int NOT NULL,
    CONSTRAINT Adocao_pk PRIMARY KEY (id)
);

-- Table: Animal
CREATE TABLE Animal (
    id SERIAL  NOT NULL,
    raca text  NOT NULL,
    cor_pelo text  NOT NULL,
    numero_patas int  NOT NULL,
    sexo text  NOT NULL,
    nome varchar(50)  NOT NULL,
    data_nascimento date  NOT NULL,
    data_entrada date  NOT NULL,
    altura int  NOT NULL,
    comprimento_pelagem int  NOT NULL,
    altura_pelagem int  NOT NULL,
    cauda text  NOT NULL,
    local_microchip text  NOT NULL,
    codigo_microchip varchar(50)  NOT NULL,
    sinais_particulares varchar(50)  NOT NULL,
    CONSTRAINT Animal_pk PRIMARY KEY (id)
);

-- Table: Cliente
CREATE TABLE Cliente (
    id SERIAL  NOT NULL,
    nome varchar(50)  NOT NULL,
    morada varchar(50)  NOT NULL,
    cod_postal varchar(40)  NOT NULL,
    localidade varchar(40)  NOT NULL,
    data_nascimento date  NOT NULL,
    data_inscricao date  NOT NULL,
    sexo text  NOT NULL,
    profissao text  NOT NULL,
    estado_civil text  NOT NULL,
    id_local int  NOT NULL,
    CONSTRAINT Cliente_pk PRIMARY KEY (id)
);

-- Table: Colaborador
CREATE TABLE Colaborador (
    id serial  NOT NULL,
    nome varchar(50)  NOT NULL,
    morada varchar(50)  NOT NULL,
    cod_postal varchar(40)  NOT NULL,
    localidade varchar(40)  NOT NULL,
    data_nascimento date  NOT NULL,
    data_inscricao date  NOT NULL,
    sexo text  NOT NULL,
    profissao text  NOT NULL,
    estado_civil text  NOT NULL,
    id_local int  NOT NULL,
    CONSTRAINT Colaborador_pk PRIMARY KEY (id)
);

-- Table: Recolha
CREATE TABLE Recolha (
    id SERIAL  NOT NULL,
    id_animal int  NOT NULL,
    id_colaborador int  NOT NULL,
    id_condicao_saude int  NOT NULL,
    id_local int  NOT NULL,
    data date  NOT NULL,
    motivo_recolha text  NOT NULL,
    CONSTRAINT Recolha_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: Adocao_Animal (table: Adocao)
ALTER TABLE Adocao ADD CONSTRAINT Adocao_Animal
    FOREIGN KEY (id_animal)
    REFERENCES Animal (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Adocao_Cliente (table: Adocao)
ALTER TABLE Adocao ADD CONSTRAINT Adocao_Cliente
    FOREIGN KEY (id_cliente)
    REFERENCES Cliente (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Adocao_Colaborador (table: Adocao)
ALTER TABLE Adocao ADD CONSTRAINT Adocao_Colaborador
    FOREIGN KEY (id_colaborador)
    REFERENCES Colaborador (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;
-- Reference: Adocao_Condicao_saude (table: Adocao)
ALTER TABLE Adocao ADD CONSTRAINT Adocao_Condicao_saude
    FOREIGN KEY (id_condicao_saude)
    REFERENCES Condicao_saude (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Recolha_Animal (table: Recolha)
ALTER TABLE Recolha ADD CONSTRAINT Recolha_Animal
    FOREIGN KEY (id_animal)
    REFERENCES Animal (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Recolha_Colaborador (table: Recolha)
ALTER TABLE Recolha ADD CONSTRAINT Recolha_Colaborador
    FOREIGN KEY (id_colaborador)
    REFERENCES Colaborador (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Recolha_Condicao_saude (table: Recolha)
ALTER TABLE Recolha ADD CONSTRAINT Recolha_Condicao_saude
    FOREIGN KEY (id_condicao_saude)
    REFERENCES Condicao_saude (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Recolha_Local (table: Recolha)
ALTER TABLE Recolha ADD CONSTRAINT Recolha_Local
    FOREIGN KEY (id_local)
    REFERENCES "local" (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Cliente_Local (table: Cliente)
ALTER TABLE Cliente ADD CONSTRAINT Cliente_Local
    FOREIGN KEY (id_local)
    REFERENCES "local" (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- Reference: Colaborador_Local (table: Colaborador)
ALTER TABLE Colaborador ADD CONSTRAINT Colaborador_Local
    FOREIGN KEY (id_local)
    REFERENCES "local" (id)
    NOT DEFERRABLE
    INITIALLY IMMEDIATE
;

-- End of file.

